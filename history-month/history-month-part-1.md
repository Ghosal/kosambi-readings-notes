# History of the Indian Left

- On Dadabhai Naoroji
- MPT Acharya: Reminisces of a Revolutionary
    - Basically: "MPT Acharya: The IRL Shipping Fic"
- The Indian Sociologist, A.M Shah
- History of the Anushilan Samiti
- Virendranath Chattopadhyay
- MN Roy and the Mexican Revolution: How a militant Indian nationalist became an International Communist
    - Nishi: In the paper on Roy and his life in Mexico the author explores the way in which his local and cultural interactions pushed him towards communism and shaped his politics for the rest of his life.
    - Nishi: 
        - the reading on the visit to Lenin is basically a love letter by Roy to Lenin. Roy is awe of what USSR has accomplished. He is in awe of the buildings, the functioning. He is critical f the time management and extremely effusive about Lenin's penchant to maintain time. He gives vivid descriptions of the man himself including one where he refers to his head as a dome. 
        - He refers to lenin's ability to maintain a unified bolshevik authoritarian front to the world while practicing democracy  within the party. he is incredibly effusive about the way in which lenin engages with him, lets down his guard and discusses important matters of the colonies with him. 
        - He writes about lenin's not knowing enough about colonies and his humility in asking Roy to fill that particular knowledge gap because of his experience in russia and his grasp on marxist theory. Roy comes away from the meeting with fond memories of the man and in his reflections compares Stalin and trotsky to robbespierre of the French revolution and blames them for the destruction of the Russian revolution.
- Naujawan Bharat Sabha and HSRA
- Formation of the CPI
    - 1920 version
    - 1925 version
- Origins of the RSP
- The Meerut Conspiracy
- Cellular jail: stories of clemency and betrayal

## Comments

- Early days of the Indian left read like a goddamn adventure novel

## Names

- Dadabhai Naoroji
- and then there's MPT Acharya, who mentions Tilak, India house, Savarkar, Dhingra- the India house crew
- Okay so continuing on the topic of Names- then we have Anushilan samiti, aurobindo ghosh, and MN Roy from the samiti
- shyamji krishnaverma- Indian sociologist
- MN Roy -> Lenin, Shaukat Usmani
- Chatto -> savarkar
- Who did SA Dange know?

## Group Discussion Points

@Prawn: Reading about the formation of CPI (the india parts). Came across this really interesting dude. Singaravelar. Founded india's first trade union. Made india's first ever May Day celebration happen. And very interestingly, was openly anti-caste - embraced buddhism, was involved in the Self Respect movement (anti-caste movement in the south)

## Unionization in Indian from 1900 to the AITUC

"In the history of the working class movement in India, Madras came to occupy an important place when, within six months of the Russian Revolution, the first formal trade union was organised in India, the Madras Labour Union, by activists like G. Selva­pathy Chetty and Thiru Vi Ka (V. Kalyana­sundaram), with the blessings of Singaravelar and others of his ilk. Then followed unions such as the M.S.M. Workers’ Union, and the Electricity Workers’ Union, the Tramway Workers’ Union, the Petroleum Employees’ Union, the Printing Workers’ Union, the Aluminium Workers’ Uni­on, the Railway Employees’ Union, the Coimbatore Wea­vers’ Union and the Madurai Weavers’ Union.

From their inception, trade unions in Madras were drawn into long, and often bloody, confrontations with the managements. The Buckingham & Carnatic Mills workers’ strike is a case in point. The management of the Mills did not even concede the workers’ right to combine. The union was banned by the British authorities. A flash point occurred when a British manager threatened workers with a gun, which was snatched by the ­workers who started firing back. The police came and opened fire, killing two workers.

The leaders of the Union called for a strike on June 21, 1921.The management retaliated by instigating a caste war through recruitment of workers from ‘low’ castes to fill the stri­kers’ vacancies. The strike turned into a caste clash between two warring groups. On August 29, 1921, police firing near the Mills’ premises in Perambur killed seven people. When their funeral procession was taken out, some miscreants threw stones, leading to another round of caste violence. Two more firings – on September 19 and October 21 – followed. After six months, the strike came to an end, failing to meet any of its objectives."