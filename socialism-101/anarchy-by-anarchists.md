# What Is Anarchy? Anarchism 101

Resource (Video) : [What is Anarchy? | Anarchism 101](https://www.youtube.com/watch?v=CDMGfsUjS_I&feature=youtu.be)

## Notes

- Anarchists held that capitalism and the state should be abolished in favour of a society in which humanity as a whole was free, equal and bonded together under relations of solidarity. They called this society anarchy.

- In advocating anarchy as their ultimate end goal, anarchists were not using the term in the sense of a "disorganized and chaotic" society, in which there is- to use Thomas Hobbes' famous phrase- "a wall of all against all". They were in fact referring to a stateless, classless and non-hierarchical society.

- In 1894, Reclus defined anarchy as "a society in which there are no more masters, no more custodians of public morals, no more jailers, torturers and executioners, no more rich or poor. Instead there will only be brothers who have their share of daily bread, who have equal rights, and who coexist in peace and heartfelt unity that comes not out of obedience to law, which is always accompanied by dreadful threats, but rather from mutual respect for the interests of all, and from the scientific study of natural laws." (Anarchy, Geography and Modernity- Reclus)

- Malatesta likewise defined anarchy in 1891 as a "society of free men" and "a society of friends". Six years later in 1897 he referred to anarchy as a "society organized without authority" where authority is understood to mean "the ability to impose one's own wishes on others through coercion". (A Long and Patient Work, Malatesta)

- Malatesta continued to espouse this definition several decades later when he wrote in 1933 that anarchy is "a society based on free and voluntary accord- a society in which no one can force his wishes on another and in which everyone can do as he pleases and together all will voluntarily contribute to the well-being of the community." (The Method of Freedom- Malatesta)