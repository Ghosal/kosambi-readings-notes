# Kosambi Group Readings on Caste

## Ambedkar: Why I Am An Unlikely President For This Conference

[Link](https://drive.google.com/file/d/1sXxDSUXU4rWCLp7vc17NwBsiv9MgnxS4/view)

Introduction from the _Reformation of Caste_ speech

Notes:

- There is a fair amount of scathing anger here, and mockery of the savarna political order to the tune of "ooooh, is an untouchable like me really the best idea to head your Very Important Caste Reform conference? don't your own brahmin strictures forbid that?"
- And in that mockery, doubt that this group could at all accomplish the stated goal of caste reform, given that (??? were they all savarnas???)

## Does Untouchability Exist Among Muslims

[Link](https://drive.google.com/file/d/1sXxDSUXU4rWCLp7vc17NwBsiv9MgnxS4/view)

Authors: Prashant K Trivedi, Srinivas Goli, Fahimuddin, Surinder Kumar

Abstract: Untouchability forms a crucial criterion for inclusion in the list of Scheduled Castes. It is rarely discussed with reference to Muslims. A household survey was conducted in 14 districts of Uttar Pradesh to examine contradictory claims about the practice of untouchability by non-Dalit Muslims and Hindus towards Dalit Muslims in Uttar Pradesh. A section of Dalit Muslim
respondents report existence of untouchability in dining relations, habitation, social interaction and access to religious places. Surprisingly, a higher proportion of non-Dalit Muslims corroborate these claims.

## Dumont, Guha and The Danger of Stereotyping India

[Link](https://thediplomat.com/2018/03/dumont-guha-and-the-danger-of-stereotyping-india/)

A criticism of both Dumont's book and his perspective. Social orders are not neat, essentialism sucks, power is as important as purity in the social orders of caste

## Beyond Caste: The Birth Of Caste, Chapter 1

[Link](https://drive.google.com/file/d/1P8vhZ1O0_ZszeUTRqPEZNsaj_MoC0tMS/view)




