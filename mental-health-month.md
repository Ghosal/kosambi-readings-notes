# Mental Health Month

## Light

    - [Where social justice meets mental health](https://mhi.org.in/voice/details/where-social-justice-meets-mental-health/) :: read
        - tags :: emotionally healing, oppressional trauma
    - [What Fanon still teaches us about mental illness in post-colonial societies](https://theconversation.com/what-fanon-still-teaches-us-about-mental-illness-in-post-colonial-societies-102426) :: skimmed, oppressional trauma

## Involved

    - [Social Exclusion and Mental Health: The Unexplored Aftermath of Caste-based Discrimination and Violence](./resources/social-exclusion-and-mental-health.pdf)
        - tags :: oppressional trauma
    - [Stop Making Sense: Alienation and Mental Health](./resources/stop-making-sense.pdf) :: skimmed
        - tags :: basics
        - mentions reagan and AIDS crisis as an example of "humans are not on the side of humans"
    - [The Managed Heart: Arlie Russel Hochschild, Chapters 6 and 7](https://caringlabor.files.wordpress.com/2012/09/the-managed-heart-arlie-russell-hochschild.pdf)
        - tags :: labour
    - [Bullshit Jobs: David Graeber, Chapters 3 and 4](https://www.rentabasicauniversal.es/wp-content/uploads/2018/09/Bullshit-Jobs_-A-Theory-David-Graeber.pdf)
        - tags :: labour
        - recommended by Vaishnavi

## The Managed Heart

Chapters 6, 7

    - "relations based on getting and giving money are to be seen _as if_ they were relations free of money"
    - "the trainees were asked to help the company create the illusion it wanted the passengers to accept"
    - the facts of capitalism- anti-union position, the tolerance of unruly customers, etc. - are taken as facts of life, and the focus is on managing one's own anger (covert ways of action will always be found)
    - collective emotion management: company trying to influence the way workers will interact with each other
    - emotional exchange used as a tool for profit

