# Mental Health Month: Second Set of Readings

- [Identifying depression symptoms among general population
living in conflict zone in Jammu and Kashmir by Anood Tariq Wani, Tafazzul Hyder Zaidi](mejfm.com/June%202020/Depression%20Jammu%20Kashmir.pdf)
    - academic paper, data
	- So like, I saw the reading serving a dual purpose- firstly, giving conclusive proof (of the kind that can be used by policymakers) that yes, people are suffering in war, in the weirdass civil-war-repression thing going on in J&K (because not everyone there is fighting actively, right, they're mostly just living in terror or adjacent to the fighting)
    - Okay and secondly- even if we know that yes, people suffer when living in a wartorn area, this is a precise study of how people suffer in a war torn area... and like, as a mental health professional, you can't exactly stop the war, right? But you can study about the ways your clients are distressed, and using that knowledge, help them form resilient bonds of community to lessen the distress
    - like... idk, it reminded me of my therapist telling me once that I was going to have a lot of shit in my life- due to being trans and transitioning- and that I needed to form stable pillars in my life, and work on resilience
    - Like, she couldn't change the transphobic society, but she could damn well make sure that I don't collapse in said society

- [A Future with No Future: Depression, the Left, and the Politics of Mental Health](https://lareviewofbooks.org/article/future-no-future-depression-left-politics-mental-health/)
    - So this one is short but good re: explaining the general sense of political despair people have

- [I’m a Dalit woman, and my mental health matters. What we need is an overhaul](https://scroll.in/article/946760/im-a-dalit-woman-and-my-mental-health-matters-what-we-need-is-an-overhaul)
    - emotionally hitting
    - so this one was technically extra readings but I find it necessary tbh

## Mad Pride Potential Notes

https://books.google.co.in/books?hl=en&lr=&id=MFwoDwAAQBAJ&oi=fnd&pg=PT19&dq=mad+pride+movement&ots=p09wSGDyou&sig=zSHHi5vC3J7hpcjmoEe2cE-RePQ&redir_esc=y#v=onepage&q=mad%20pride%20movement&f=false

https://www.tandfonline.com/doi/abs/10.3109/01612840.2012.740769?journalCode=imhn20

https://jemh.ca/issues/v9/documents/JEMH_Open-Volume_Article_Angry_and_Mad-Oct2015.pdf

https://books.google.co.in/books?hl=en&lr=&id=Im1IdyxmH_4C&oi=fnd&pg=PA115&dq=mad+pride+movement&ots=rqVnGo5Cga&sig=59Rxkv6too8enlaIQlEQDujD3yc&redir_esc=y#v=onepage&q=mad%20pride%20movement&f=false

https://books.google.co.in/books?hl=en&lr=&id=_eHaAAAAQBAJ&oi=fnd&pg=PP1&dq=mad+pride+movement&ots=ZBM1QOoS53&sig=WMznyEJzuM7BwNZnhVIHfg3JSRo&redir_esc=y#v=onepage&q=mad%20pride%20movement&f=false

https://www.tandfonline.com/doi/abs/10.1080/02604027.2020.1730737

https://www.tandfonline.com/doi/full/10.1080/09687599.2019.1692168

https://link.springer.com/chapter/10.1007/978-3-030-23331-0_10

https://www.healthaffairs.org/doi/full/10.1377/hlthaff.25.3.720

https://web.a.ebscohost.com/abstract?direct=true&profile=ehost&scope=site&authtype=crawler&jrnl=14479532&AN=50282505&h=Ou%2byEE3uGB2nikUvmweFermS1oArIcxMRh7XNT1DU5jFDl4LdiaSVBnjO1SjgMsoxvtWAwj9NWrXC7vMV3B1WA%3d%3d&crl=c&resultNs=AdminWebAuth&resultLocal=ErrCrlNotAuth&crlhashurl=login.aspx%3fdirect%3dtrue%26profile%3dehost%26scope%3dsite%26authtype%3dcrawler%26jrnl%3d14479532%26AN%3d50282505

https://www.sciencedirect.com/science/article/abs/pii/S135382920000023X
