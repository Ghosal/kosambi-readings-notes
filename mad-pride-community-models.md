
On a different note- this is not strictly related to socialism, but I wanted to bring up the mad pride movements and that general line of ideology about being anti-uniformity and the movement to make being visibly different okay

(Bringing this up now because I finally collated the mad pride readings I wanted to bring up :P)

So a lot of the readings this month focus on community models of mental health, and community models of healing, which is extremely valuable and good and needs to be talked about, yes

But I think there hasn't been enough questioning about what exactly this concept of "community" means. And also, perspectives of people who feel alien and not normal and not connected to their community- not alienated in the marxist sense, alienated in the sense of being visibly different.

(I am trying so hard to not use "crazy", lol :P like sort of the point of me bringing this up is that okay, most people have mental issues under capitalism yes, but what about those of us who are in fact crazy? the nutters, y'know? the visibly people-get-creeped-out-being-around-you different)

... actually this is connected to one of the readings- Disorder- Contemporary Fascism and the Crisis in Mental Health mentions foucault and his criticism of the construct of "mentally ill"- foucault states that the very notion of "sanity" or "normalcy" is a bourgeois notion, so what's defined as "sane" under a capitalist society is heavily dictated by the ideal society of the bourgeoisie

reading: https://helenfarabee.org/poc/view_doc.php?type=book&id=2239&cn=144

***

So like, my history with mad pride is that I am- fundamentally- very weird? and a part of that weirdness is that I have a bunch of pretty stigmatized diagnoses (i have a diagnosis soup, what the hell even are diagnoses tbh) and I kind of spent a while rattling through the medical system trying to cure the bits that could be cured... and coming to terms with living permanently and weirdly with the bits that either could not, or that I did not want to be "cured", or for which the price for "curing" was getting horrible rare medical side-effects (hello antipsychotics. y u hate me)

And like, the consequence of living with this kind of brain is that I'm fundamentally weird, different- like, yes all people are mentally ill, but not all people go nonverbal every so often, or freeze and can't move, or have voices in their head. And like, coming to terms with living with all of the weirdness sort of meant accepting that this __is__ my normal, and that okay maybe for some people having a bad day means ice cream and lots of crying, and for me a bad day means bugs crawling all over my skin and speaking in broken words (and ice cream and a lot of crying) and that that's normal and I should be able to tell people that without them telling me "oh no that's terrible, please go to a psych". I need space to exist - exist in distress! exist and need care! - without being someone whose existence is __defined__ by care and distress.

So that's one of the things I wanted to point out by bringing up mad pride- normalize madness, so that it helps those of us who are "mad".

as for the points raised re: agency: so like, there's the difference between listening to individual mentally ill people and mentally ill people as a class? although- we should listen to individually mentally ill people as much as possible within reason, yes, and some of us have fundamentally different means of communication- our communication can be crying or freezing or meltdowns or broken words, and part of the mad pride argument is that you must respect us as people __even when we're communicating this way__ .

but also- listening to mentally ill people as a class! like, listen to your communicating patients! and cross-apply the techniques to your patients who aren't able to coherently communicate! (very often these are the same patient)

