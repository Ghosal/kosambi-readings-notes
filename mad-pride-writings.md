# Mad Pride

Mad pride, and how it relates to the treatment of mental health.

This is in no way an exhaustive set of readings- this is honestly picked up by going to Google Scholar, googling Mad Pride and picking up the readings that I thought were valid- because I was introduced to the concept of mad pride and it's-okay-to-be-crazy via forums, mostly.

- [Mad Pride: Reflections on Sociopolitical Identity and Mental Diversity in the Context of Culturally Competent Psychiatric Care](https://www.tandfonline.com/doi/abs/10.3109/01612840.2012.740769?journalCode=imhn20)
